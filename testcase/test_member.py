#!/usr/bin python3
# -*- encoding: utf-8 -*-
"""
@author: 关河九州
@contact: 
@software: PyCharm
@file: test_member
@time: 2022/2/28 15:18
"""
import os

import allure
import pytest
import yaml
from allure_commons._allure import severity
from allure_commons.types import Severity
from loguru import logger

from common.handle_path import DATA_DIR
from page.member_page import MemberPage

""" =============== 成员模块测试 =============== """

case_data_path = os.path.join(DATA_DIR, 'member_data.yaml')
datas = yaml.safe_load(open(case_data_path, encoding='utf-8'))


@allure.feature("成员模块")
class TestMember(MemberPage):
    conf_mysql = MemberPage().mysql_conf

    @allure.story('登录')
    @allure.title('{data[title]}')
    @severity(Severity.BLOCKER)
    @pytest.mark.parametrize('data', datas['login'])
    def test_login(self, data):
        """
        登录验证
        :param data:
        :return:
        """
        result = self.login_api(**data['account']).json()
        self.assert_equal(data['expected']['token_type'], result['token_type'])
        # self.assert_equal(data['expected']['Response message'], result['Response message'])
        logger.info("登录验证用例通过！")

    @allure.story('充值')
    @allure.title('{data[title]}')
    # indirect=False时，argnames参数被当成普通变量
    # indirect=True时，parametrize中的argnames参数被当成函数执行
    @pytest.mark.parametrize('connect_mysql', [conf_mysql], indirect=True)
    @pytest.mark.parametrize('data', datas['recharge'])
    def test_recharge(self, data, get_login_data, connect_mysql):
        """
        充值业务验证
        :param data:
        :param get_login_data: 获取登录接口数据
        :param connect_mysql:
        :return:
        """
        login_data = get_login_data
        db = connect_mysql
        # 调用充值接口，返回结果
        result = self.page_recharge(data, login_data, db)
        # 断言
        self.assert_equal(data['expected']['code'], result['code'])
        self.assert_contains(data['expected']['msg'], result['msg'])
        if data['sql']:
            self.assert_equal(self.to_two_decimal(data['amount']), result['recharge'])
        logger.info('用例通过！')
