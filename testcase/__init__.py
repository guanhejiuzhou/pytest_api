#!/usr/bin python3
# -*- encoding: utf-8 -*-
"""
@author: 关河九州
@contact: 
@software: PyCharm
@file: __init__.py
@time: 2022/2/24 15:59
"""

""" =================== 用例层 ==================== """
'''这一层，通过调用不同的业务来完成相关测试。用例层不关心底层逻辑，只关心测试逻辑，业务场景如何测试，如何断言'''
