#!/usr/bin python3
# -*- encoding: utf-8 -*-
"""
@author: 关河九州
@contact: 
@software: PyCharm
@file: handle_assert
@time: 2022/2/23 15:20
"""
import allure
from loguru import logger

""" =============== 断言封装 =============== """


class HandleAssert:

    @staticmethod  # 不需要实例化，直接类名.方法名()来调用，不需要表示自身对象的self和自身类的cls参数
    @allure.step("断言")
    def eq(ex, re):
        """
        断言相等
        :param ex: 预期结果
        :param re: 实际结果
        :return:
        """
        try:
            assert str(ex) == str(re)
        except AssertionError as e:
            logger.error(f"eq断言失败，预期结果：{ex}，实际结果：{re}")
            logger.error("用例失败！")
            raise e

    @staticmethod
    def contains(content, target):
        """
        断言包含
        :param content: 文本内容
        :param target: 目标文本
        :return:
        """
        try:
            assert str(content) in str(target)
        except AssertionError as e:
            logger.error(f"contains断言失败，目标文本{target}包含 文本{content}")
            raise e
