#!/usr/bin python3
# -*- encoding: utf-8 -*-
"""
@author: 关河九州
@contact: 
@software: PyCharm
@file: run
@time: 2022/2/24 17:06
"""
import os

import pytest
from loguru import logger

""" =============== 测试用例运行主程序 =============== """
logger.info("""
                     _    _         _      _____         _
      __ _ _ __ (_)  / \\  _   _| |_ __|_   _|__  ___| |_
     / _` | '_ \\| | / _ \\| | | | __/ _ \\| |/ _ \\/ __| __|
    | (_| | |_) | |/ ___ \\ |_| | || (_) | |  __/\\__ \\ |_
     \\__,_| .__/|_/_/   \\_\\__,_|\\__\\___/|_|\\___||___/\\__|
          |_|
        """)

if __name__ == '__main__':
    # pytest.main(["-m", "blocker"])
    # rotation=20MB 文件大于20MB就会重新生成一个文件
    # retention=1 week  一周后会清空日志
    logger.add('./log/{time}.log', rotation='20 MB', retention='1 week', encoding='utf-8')
    # 以alluredir运行生成报告，保存在result文件夹中
    pytest.main(['-s', r"--alluredir=report/result", "--clean-alluredir"])
    # 将报告转换成html格式文件
    os.system('allure generate ./report/result -o ./report/html -c')
