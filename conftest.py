#!/usr/bin python3
# -*- encoding: utf-8 -*-
"""
@author: 关河九州
@contact: 
@software: PyCharm
@file: conftest
@time: 2022/2/24 16:39
"""
import pytest
from loguru import logger

from api.member_api import MemberApi
from common.handle_mysql import HandleMysql

""" =============== 前置条件处理 =============== """

'''
scope参数：标记方法的作用域，function(默认、函数)、class(类)、module(模块)、session(包)
function：该模块中所有的函数在执行前都会执行
class：在类中都只会被执行一次
module：整个模块中都只会执行一次
session：整个包中只会执行一次
autouse=True：自动调用fixture功能
'''


# 为了避免频繁登录，级别可以根据需求设置
# 这里整个class只会调用一次登录
@pytest.fixture(scope='class')
def get_login_data():
    """获取登录数据"""
    data = MemberApi().get_login_data()
    # 这里返回的不是`token`，而是整个登录响应结果（字典），是因为其它接口还可能需要除token外的其它登录信息，而其它接口需要什么登录信息，直接字典取值即可。
    return data


@pytest.fixture(scope='class')
def connect_mysql(request):
    """连接数据库"""
    db = HandleMysql(**request.param)
    yield db
    db.close()


@pytest.fixture(scope='session', autouse=True)
def task_mark():
    logger.debug("{:=^50}".format('测试任务开始'))
    yield
    logger.debug("{:=^50}".format('测试任务结束'))


@pytest.fixture(autouse=True)
def case_mark():
    logger.debug("{:=^50}".format('用例开始'))
    yield
    logger.debug("{:=^50}".format('用例结束'))
